# travel-packlist

## Sokhetes utazásra, mosási lehetőséggel
### Előre elkészítendő
 - [ ] ESTA
 - [ ] Repülőjegy oda/vissza
 - [ ] Autóbérlés papírjai
 - [ ] Biztosítás kártya
 - [ ] CodeOne/Oracle jegy

### Kézipoggyász
 - [ ] Laptop
 - [ ] Egér
 - [ ] Laptop töltő, tablet/telefon töltő (USB2-TypeC kábel, USB2-mikroUSB kábel, fali adapter)
 - [ ] Tablet
 - [ ] Hosszabbító
 - [ ] Jack-kábel
 - [ ] PowerBank
 - [ ] fesztivál telefon
 - [ ] Kindle
 - [ ] Fényképezőgép, töltővel
 - [ ] Jogosítvány
 - [ ] Lakcímkártya
 - [ ] Útlevél
 - [ ] 1 boxeralsó
 - [ ] 1 pár zokni
 - [ ] 1 póló
 - [ ] hajgumi
 - [ ] nyakpárna

### Feladott poggyász
 - [ ] 5 boxer
 - [ ] 5 pár zokni
 - [ ] 1 fürdőgatya
 - [ ] 1 esőkabát
 - [ ] 1/2 rövidnadrág (időjárás függő)
 - [ ] 1/2 hosszú nadrág (időjárás fügő)
 - [ ] 6 póló/ing
 - [ ] 1/2 pulóver (időjárás függő)
 - [ ] 1/2 törülköző
 - [ ] papucs

## Néhány napos utazásra
 - [ ] n+(1-2) gatya
 - [ ] n+(1-2) zokni
 - [ ] n+1 ing
 - [ ] 1 póló
 - [ ] 1 nadrág
 - [ ] 1 esőkabát
 - [ ] törülköző
 - [ ] telefontöltő
 - [ ] jegyek
 - [ ] iratok
 - [ ] pénz